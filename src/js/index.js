const $ = require('jquery');
const echarts = require('echarts');
require('switcheryCss');
const switchery = require('switchery');

$(function () {
  switchery($('.js-switch')[0], {size: 'large'});
  $('.demo-text').text('Hello jQuery!');
  $('#loadChart').click(function () {
    let myChart = echarts.init(document.getElementById('myChart'));
    myChart.setOption({
      title: {text: 'ECharts 入门示例'},
      tooltip: {},
      xAxis: {
        data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
      },
      yAxis: {},
      series: [{
        name: '销量',
        type: 'bar',
        data: [5, 20, 36, 10, 20, 20]
      }]
    });
  });
});

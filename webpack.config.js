'use strict';
let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

let plugins = [
  new webpack.optimize.CommonsChunkPlugin(
    {
      name: ['jquery', 'echarts'],   // 将公共模块提取
      filename: 'common.js',
      minChunks: Infinity // 提取所有entry公用依赖的模块
    }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery'
  }),
  new webpack.IgnorePlugin(/src\/libs\/*/),
  new ExtractTextPlugin('[name].[contenthash:9].css')
];

module.exports = {
  cache: true,
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true
  },
  entry: {
    server: 'webpack/hot/dev-server',
    client: 'webpack-dev-server/client?http://localhost:8080',
    'index': './src/js/index.js'
  },
  plugins: plugins,
  output: {
    path: __dirname + '/dist/js',
    filename: '[name].js',
    publicPath: './js/'
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: 'style!css-loader'},
      {test: /\.js$/, loader: 'babel?presets[]=es2015'},
      {test: /\.less$/, loader: 'style!css!less?sourceMap'},
      {test: /\.(gif|png|jpg|jpeg)$/, loader: 'url?limit=8192&name=images/[name].[ext]'},
      {test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'url'}
    ]
  },
  resolve: {
    root: __dirname,
    extensions: ['', '.js', '.json', '.less'],
    alias: {
      jquery: 'src/lib/jquery-1.12.4.min.js',
      echarts: 'src/lib/echarts.min.js',
      switchery: 'src/lib/switchery/switchery.js',
      switcheryCss: 'src/lib/switchery/switchery.min.css'
    }
  },
  externals: {
    '$': 'jquery'
  }
};
